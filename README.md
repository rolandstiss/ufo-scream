# Ufo Scream

A voice activated mobile platformer. Ufo Scream is a mobile game prototype in which ufo goes from side to side
and decreases an altitude. To survive and land safely it must shoot all the buildings below with a voice activated
gun which is placed on the ship. A small fun prototype with a potential for something bigger maybe.

## Getting Started

Checkout master branch to have the project locally

### Prerequisites

To run and build the project Unity 2019.2.0f1
To build for Android - Android SDK must be installed
Since this is voice activated game a mic is a must.

### Run the Project

Open project with Unity and press "play" button to test the game.

*make sure you have a microphone connected to your system

## Running the tests

Explain how to run the automated tests for this system

## Built With

Default Unity packages.

## Authors

Game Idea, Game Design, code, art style, etc. - Rolands Tiss

## License

CC0

## Acknowledgments

Explosion VFX - https://opengameart.org/sites/default/files/Explosion_2.png?fbclid=IwAR18rCgVV7JcT-LkYHO4TKH6IBjalmsXV2y2lOIFz95tcfkLi4qEltIgeo8
Laser SFX - https://freesound.org/people/V-ktor/sounds/435416/
Explosion SFX - https://freesound.org/people/Apenguin73/sounds/335152/
Background Music - https://www.looperman.com/loops/detail/143541/ufo-landing-131-131bpm-trap-synth-loop
