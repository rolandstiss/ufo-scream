﻿/*
 * Script for debug purposes to see mic input level
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeGrower : MonoBehaviour
{
    public float maxSize;

    void Start()
    {
        maxSize = 1000;
    }

    // Update is called once per frame
    void Update()
    {        
        this.transform.localScale = new Vector3(0.2f, MicrophoneController.amplifiedInputVolume, 0.2f);
    }
}
