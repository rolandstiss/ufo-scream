﻿/*
 * This script drives each buildings sprites and destroy states
 */ 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingController : MonoBehaviour
{
    GameController _gc;

    public GameObject explosionPE;
    public Sprite[] buildingSprites;

    SpriteRenderer _buildingImage;
    BoxCollider2D _collisionBox;

    int _spriteAmount;
    int _currentSprite;
    bool _buildingDestroyed;

    private void Start()
    {
        _currentSprite = 0;

        _buildingImage = GetComponent<SpriteRenderer>();
        _collisionBox = GetComponent<BoxCollider2D>();
        _buildingImage.sprite = buildingSprites[_currentSprite];

        _spriteAmount = buildingSprites.Length;
        _buildingDestroyed = false;

        SetCollisionBox();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //play explosion vfx here
        GameObject explosionVFX = Instantiate(explosionPE, new Vector2(collision.contacts[0].point.x, collision.contacts[0].point.y - 0.5f), Quaternion.identity) as GameObject;

        if (collision.gameObject.tag == "Projectile")
        {
            GameObject _projectile = collision.gameObject;
            _projectile.GetComponent<ProjectileController>().startDestroying = true;

            if (_currentSprite < _spriteAmount - 1)
            {
                _currentSprite++;
                _buildingImage.sprite = buildingSprites[_currentSprite];
                SetCollisionBox();

                if (_currentSprite == _spriteAmount - 1 && _buildingDestroyed != true)
                {
                    GameController.destroyedBuildingCount++;
                    _buildingDestroyed = true;
                    _collisionBox.enabled = false;
                }
            }            
        }

        if(collision.gameObject.tag == "Player")
        {
            Destroy(collision.gameObject);
            //set game over here
            GameController.gamePaused = true;
            GameController.gameOver = true;
        }
    }

    void SetCollisionBox()
    {
        Vector2 S = _buildingImage.sprite.bounds.size;
        _collisionBox.size = S;
        _collisionBox.offset = new Vector2(0, S.y / 2);
    }
}
