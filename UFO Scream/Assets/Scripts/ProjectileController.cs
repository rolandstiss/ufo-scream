﻿/*
 * This script moves projectile towards buildings
 */ 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{
    public float projectileSpeed;
    public bool startDestroying;

    AudioSource _audioSource;
    SpriteRenderer _sr;
    CircleCollider2D _cc;

    private void Start()
    {
        startDestroying = false;
        _sr = GetComponent<SpriteRenderer>();
        _audioSource = GetComponent<AudioSource>();
        _cc = GetComponent<CircleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameController.gamePaused)
        {
            transform.Translate(Vector2.up * -projectileSpeed * Time.deltaTime);

            //on collision disable sprite, collider and check if sound clip has ended
            if(startDestroying)
            {
                _sr.enabled = false;
                _cc.enabled = false;
                DestroyProjectile();
            }
        }
    }

    //Destroy projectile when audio clip ends to avoid sound clipping
    void DestroyProjectile()
    {
        if (!_audioSource.isPlaying)
        {
            Destroy(this.gameObject);
        }
    }
}
