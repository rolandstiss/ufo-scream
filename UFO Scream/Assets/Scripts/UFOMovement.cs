﻿/*
 * This script is responsible for moving the player from side to side and down
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UFOMovement : MonoBehaviour
{
    float _landingSpeed;
    float _horizontalSpeed;
    float _verticalSpeed;
    float _verticalSpeedIncrement;
    float _t;

    bool _moveToLeft;

    Camera _gameCamera;
    Vector2 _lastPos;
    SpriteRenderer _ufoSprite;

    void Start()
    {
        GameController _gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        _moveToLeft = true;
        _gameCamera = _gc.gameCamera;
        _horizontalSpeed = _gc.horizontalPlayerSpeed;
        _verticalSpeed = _gc.verticalPlayerSpeed;
        _verticalSpeedIncrement = _gc.verticalPlaerSpeedIncrement;
        _landingSpeed = _gc.landingSpeed;
        _ufoSprite = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        //moving player from one side of the screen to another
        //while game is on
        if (!GameController.gamePaused && !GameController.gameWon)
        {
            Vector2 _ufoPosition = _gameCamera.WorldToViewportPoint(transform.position);

            if (_ufoPosition.x >= 1.0F && !_moveToLeft)
            {
                SwitchDirection();
            }
            else if (_ufoPosition.x <= 0.0F && _moveToLeft)
            {
                SwitchDirection();
            }

            transform.Translate(Vector2.right * -_horizontalSpeed * Time.deltaTime);
            transform.Translate(Vector2.up * -_verticalSpeed * Time.deltaTime);

            //registers last position in case of winning
            _lastPos = transform.position;
        }

        //landing if all the buildings are destroyed
        if (GameController.gameWon)
        {
            _t += Time.deltaTime * _landingSpeed;
            transform.position = Vector2.Lerp(_lastPos, BuildingSpawner.screenBottomCenter + new Vector2(0f, _ufoSprite.sprite.bounds.size.y / 2), _t); 
        }

        //switch movement direction and increase vertical speed once the player reaches an edge
        void SwitchDirection()
        {
            _horizontalSpeed *= -1;
            _moveToLeft = !_moveToLeft;
            _verticalSpeed += _verticalSpeedIncrement;
        }
    }
}
