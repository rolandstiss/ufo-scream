﻿/* This script takes care of main game features as pause_on, booleans for UI menus, microphone sensitivity, input method etc.*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static bool gamePaused;
    public static bool gameOver;
    public static bool showOptions;
    public static bool gameWon;
    public static int destroyedBuildingCount;
    public static int placedBuildingCount;
    public static float microphoneSensitivity;

    public float horizontalPlayerSpeed;
    public float verticalPlayerSpeed;
    public float verticalPlaerSpeedIncrement;
    public float landingSpeed;
    public float reloadTimer;
    public float volumeThreshold;
    public Camera gameCamera;

    [HideInInspector]
    public int inputMethod; //0 - mic, 1 - tap

    [HideInInspector]
    public Vector3 ufoStartPosition;

    public GameObject ufoPrefab;
    public Slider sldSensitivity;

    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        ufoStartPosition = ufoPrefab.transform.position;
        gamePaused = false;
        gameOver = false;
        showOptions = false;
        inputMethod = 0;
        placedBuildingCount = 0;
        destroyedBuildingCount = 0;
        gameWon = false;

        microphoneSensitivity = sldSensitivity.value;

        InstantiatePlayer();
    }

    private void Update()
    {
        if (destroyedBuildingCount == placedBuildingCount)
        {
            gameWon = true;
            Debug.Log("game won!");
        }
    }

    public static void QuitGame()
    {
        Application.Quit();
    }

    public void InstantiatePlayer()
    {
        GameObject ufo = Instantiate(ufoPrefab, ufoStartPosition, Quaternion.identity) as GameObject;
        //Debug.Log("instantiate palyer");
    }
}
