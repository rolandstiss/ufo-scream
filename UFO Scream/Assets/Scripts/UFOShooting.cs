﻿/*
 * This script takes care of projectile instantiating and movement
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UFOShooting : MonoBehaviour
{
    public Transform gunPosition;
    public GameObject projectile;

    float _timeOfTheLastBullet;
    bool _scream;
    bool _tap;
    GameController _gc;

    private void Start()
    {
        _gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        _scream = true;
        _tap = false;
    }

    void Update()
    {
        //according to chosen input type, check for trigger value to shoot if game is not paused
        _scream = ((MicrophoneController.amplifiedInputVolume > _gc.volumeThreshold) && ((_timeOfTheLastBullet + _gc.reloadTimer) < Time.time) && (_gc.inputMethod == 0));
        _tap = ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) && ((_timeOfTheLastBullet + _gc.reloadTimer) < Time.time) && (_gc.inputMethod == 1));

        if (!GameController.gamePaused)
        {
            if (_tap || _scream)
            {
                _timeOfTheLastBullet = Time.time;
                GameObject _bullet = Instantiate(projectile, gunPosition.position, Quaternion.identity) as GameObject;
            }
        }
    }
}
