﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class MicrophoneController : MonoBehaviour
{
    private string _inputMicrophone;
    private AudioSource _audioSource;
    private GameController _gc;

    public int sampleRate;
    public float[] spectrum;
    public static float[] bands;
    public float averageInputVolume;
    public static float amplifiedInputVolume;

    public AudioMixerGroup _mixerGroupMicrophone;

    private int _microphoneDisabled;

    // Start is called before the first frame update
    void Start()
    {
        _gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        sampleRate = 128;
        averageInputVolume = 0.0f;
        bands = new float[6];
        spectrum = new float[sampleRate];

        StartMicrophone();
        _microphoneDisabled = _gc.inputMethod;
    }

    // Update is called once per frame
    void Update()
    {
        if (_microphoneDisabled != _gc.inputMethod)
        {
            _microphoneDisabled = _gc.inputMethod;

            if (_microphoneDisabled == 0)
                UnmuteAudioClip();
            else
                MuteAudioClip();
        }

        if (_microphoneDisabled == 0)
        {
            GetSamples();
            //GetBands();
            GetAverageInputVolume();
        }
    }

    void StartMicrophone()
    {
        _inputMicrophone = Microphone.devices[0];
        _audioSource = GetComponent<AudioSource>();
        _audioSource.outputAudioMixerGroup = _mixerGroupMicrophone;
        _audioSource.clip = Microphone.Start(_inputMicrophone, true, 999, 44100);
        _audioSource.Play();
    }

    public static void StopMicrophone()
    {
        Microphone.End(null);
    }

    void MuteAudioClip()
    {
        _audioSource.mute = true;
    }

    void UnmuteAudioClip()
    {
        _audioSource.mute = false;
    }

    void GetSamples()
    {
        _audioSource.GetSpectrumData(spectrum, 0, FFTWindow.Blackman);
    }

    void GetBands()
    {
        /*
         * this is meant for filtering high and low frequencies
         * 
         * !!!NOT used at the moment but working
         * 
         * 22500Hz/128samples = ~176Hz per sample
         * human voice range according to wikipedia 350Hz - 5kHz
         * 
         * bandId |samples| range
         * 0        2       0 - 352Hz
         * 1        4       353 - 1057Hz
         * 2        8       1058 - 2466 Hz
         * 3        16      2467 - 5282 Hz
         * 4        32      5283 - 10915 Hz
         * 5        64      10916 - 22180 Hz
         */

        for (int i = 0; i < 6; i++)
        {
            float _averageValue = 0.0f;
            int _sampleCount = (int)Mathf.Pow(2, i) * 2;

            for(int j = 0; j < _sampleCount; j++)
            {
                _averageValue += spectrum[j + (_sampleCount-2)];
            }
            bands[i] = _averageValue/_sampleCount;
        }
    }

    void GetAverageInputVolume()
    {
        foreach(float i in spectrum)
        {
            averageInputVolume += i;
        }

        averageInputVolume /= spectrum.Length;
        amplifiedInputVolume = averageInputVolume * GameController.microphoneSensitivity;
        //Debug.Log(GameController.microphoneSensitivity);
    }
}
