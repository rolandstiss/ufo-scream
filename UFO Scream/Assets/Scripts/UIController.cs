﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public Text txtAudioLvl;

    public GameObject btnSettings;
    public GameObject panelSettings;
    public GameObject panelOptions;

    // Update is called once per frame
    void Update()
    {
        txtAudioLvl.text = "Audio lvl: " + MicrophoneController.amplifiedInputVolume;

        if (!GameController.gamePaused)
        {
            btnSettings.SetActive(true);
            panelSettings.SetActive(false);
            panelOptions.SetActive(false);
        }
        else if (GameController.gamePaused && !GameController.showOptions)
        {
            btnSettings.SetActive(false);
            panelSettings.SetActive(true);
            panelOptions.SetActive(false);
        }

        else if (GameController.gamePaused && GameController.showOptions)
        {
            panelSettings.SetActive(false);
            panelOptions.SetActive(true);
        }
    }
}
