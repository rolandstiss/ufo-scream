﻿/*
 * This script controls all buttons and drives UI accordingly
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{
    public Dropdown ddInputMethod;
    public Slider sldSensitivity;

    GameController _gc;
    BuildingSpawner _bs;

    private void Start()
    {
        _gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        _bs = GameObject.FindGameObjectWithTag("GameController").GetComponent<BuildingSpawner>();
    }

    public void OnSettingsClick()
    {
        GameController.gamePaused = true;
    }

    public void OnResumeClick()
    {
        GameController.gamePaused = false;
    }

    public void OnRestartClick()
    {
        //destroy all existing projectiles
        GameObject[] _projectiles = GameObject.FindGameObjectsWithTag("Projectile");
        foreach(GameObject _projectile in _projectiles)
        {
            Destroy(_projectile);
        }

        //destroy all existing buildings
        GameObject[] _buildings = GameObject.FindGameObjectsWithTag("Building");
        foreach (GameObject _building in _buildings)
        {
            Destroy(_building.gameObject);
        }

        //destroy player object
        Destroy(GameObject.FindGameObjectWithTag("Player"));

        //if game over the ufo must be reinstantianed
        if (GameController.gameOver)
        {
            GameController.gameOver = false;
        }

        //restore start setup
        GameController.placedBuildingCount = 0;
        _gc.InstantiatePlayer();
        _bs.SpawnBuildings();
        GameController.destroyedBuildingCount = 0;
        GameController.gameWon = false;
        GameController.gamePaused = false;
    }

    public void OnOptionsClick()
    {
        GameController.showOptions = true;
    }

    public void OnBackClick()
    {
        GameController.showOptions = false;
    }

    public void OnInputMethodClick()
    {
        _gc.inputMethod = ddInputMethod.value;
    }

    public void OnQuitClick()
    {
        GameController.QuitGame();
    }

    public void OnSensitivityChanged()
    {
        GameController.microphoneSensitivity = sldSensitivity.value;
    }
}
