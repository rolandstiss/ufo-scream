﻿/*
 * Spawns buildings in the scene and re-center them to look better
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BuildingSpawner : MonoBehaviour
{
    public float xOffset;
    public GameObject buildingParent;
    public GameObject referenceGuide;
    public GameObject[] buildingGameObjects;
    public static Vector2 screenBottomCenter;

    float _leftOffset;
    float _rightOffset;
    float _nextBuilding;

    GameController _gc;
    Vector2[] _boundingBoxes;

    Dictionary<GameObject, Vector2> _buildings;

    void Start()
    {
        _leftOffset = 0.0f;
        _rightOffset = 0.0f;

        _boundingBoxes = new Vector2[buildingGameObjects.Length];
        _buildings = new Dictionary<GameObject, Vector2>();

        _gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

        //Create dictionary with sprites and their size of each building
        for (int i = 0; i < buildingGameObjects.Length; i++)
        {
            SpriteRenderer _bldSprite = buildingGameObjects[i].GetComponent<SpriteRenderer>();
            _buildings.Add(buildingGameObjects[i], _bldSprite.sprite.bounds.size);
        }

        FindBottomCenter();
        SpawnBuildings();        
    }

    void FindBottomCenter()
    {
        Camera _gameCamera = _gc.gameCamera;

        while (_gameCamera.WorldToViewportPoint(referenceGuide.transform.position).y > 0.0f)
        {
            if (_gameCamera.WorldToViewportPoint(referenceGuide.transform.position).y > 0.0f)
            {
                referenceGuide.transform.Translate(Vector2.up * -0.1f);
            }
        }

        screenBottomCenter = referenceGuide.transform.position;
    }

    public void SpawnBuildings()
    {
        ShuffleDictionary();

        //Spawn buildings with an offset from the middle
        for (int i = 0; i < _buildings.Count; i++)
        {
            Vector2 _bBox = new Vector2(_buildings.ElementAt(i).Value.x, 0);
            Vector2 _pos;

            if (i % 2 == 0)
            {
                _pos = screenBottomCenter + _bBox / 2 + new Vector2(_rightOffset, 0);
                _rightOffset += _bBox.x + xOffset;

                if (i == 0)
                {
                    _leftOffset = _rightOffset = _bBox.x / 2 + xOffset;
                    _pos = screenBottomCenter;
                }
            }
            else
            {
                _pos = screenBottomCenter - (_bBox / 2 + new Vector2(_leftOffset, 0));
                _leftOffset += _bBox.x + xOffset;
            }

            GameObject _bld = Instantiate(_buildings.ElementAt(i).Key, _pos, Quaternion.identity, buildingParent.transform) as GameObject;
            GameController.placedBuildingCount++;
        }

        RecenterBuildings();
    }

    void ShuffleDictionary()
    {
        System.Random _rand = new System.Random();
        _buildings = _buildings.OrderBy(x => _rand.Next()).ToDictionary(item => item.Key, item => item.Value);
    }

    void RecenterBuildings()
    {
        GameObject _bh = GameObject.FindGameObjectWithTag("BuildingHolder");
        _bh.transform.position = Vector2.zero;

        Transform[] _g = _bh.GetComponentsInChildren<Transform>();

        float _x1 = _g[_g.Length - 1].gameObject.GetComponent<SpriteRenderer>().sprite.bounds.size.x/2;
        float _posX1 = _g[_g.Length - 1].position.x + _x1;

        float _x2 = _g[_g.Length - 2].gameObject.GetComponent<SpriteRenderer>().sprite.bounds.size.x / 2;
        float _posX2 = _g[_g.Length - 2].position.x - _x2;

        float _mid = (_posX1 + _posX2)/2;

        _bh.transform.position = new Vector2(-_mid, 0.0f);
    }
}
