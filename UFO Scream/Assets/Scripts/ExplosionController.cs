﻿/*
 * Destroys explosion vfx object once the sound clip ends
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionController : MonoBehaviour
{
    AudioSource _as;

    private void Start()
    {
        _as = GetComponent<AudioSource>();
    }
    // Update is called once per frame
    void Update()
    {
        if(!_as.isPlaying)
        {
            Destroy(this.gameObject);
        }
    }
}
